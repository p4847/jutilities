# Utilities
A set of commonly used utilities for Golang projects.

## Why?
Allows me to use the same common functions that are present in many applications I've written,
without rewriting them time-and-time-again. Also allows for write-once-test-once look.

## Who?
Anyone can use these under MIT Licence.
