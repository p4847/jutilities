package jutilities

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func Test_security_GenerateOtp(t *testing.T) {
	issuer := "jutilities"
	_ = os.Setenv("OTP_SEED", "I5XWE2KAKNSXMZLOGZ2HO3Y")
	logger := NewLogger()

	t.Run("Generate TOTP", func(t *testing.T) {
		s := NewOtp(logger)
		OtpUri := s.GenerateTotp(issuer, "tester")
		logger.Info().Msg(OtpUri)
		assert.NotNil(t, OtpUri)
	})
	t.Run("Generate TOTP", func(t *testing.T) {
		s := NewOtp(logger)
		OtpUri := s.GenerateTotp(issuer, "jack.roden412@gmail.com")
		logger.Info().Msg(OtpUri)
		assert.NotNil(t, OtpUri)
	})
	t.Run("Verify TOTP - assert false", func(t *testing.T) {
		s := NewOtp(logger)
		assert.False(t, s.VerifyTotp("000000"))
	})
}

