package jutilities

import (
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewSecurity(t *testing.T) {

	tests := []struct {
		name   string
		logger *zerolog.Logger
	}{
		{
			"Asserts new security created where logger passed",
			NewLogger(),
		},
		{
			"Asserts new security create where no logger passed",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSecurity(nil); got != nil {
				assert.NotNil(t, got)
			}
		})
	}
}

func Test_security_ApplySha256(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name string
		data string
	}{
		{
			"Asserts value can be hashed",
			"testInput",
		},
		{
			"Asserts value cannot be hashed with 0 string",
			"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.ApplySha256(tt.data); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.data, got)
			}
		})
	}
}

func Test_security_Applysha256Bytes(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name string
		data []byte
	}{
		{
			"Asserts value can be hashed",
			[]byte("testInput"),
		},
		{
			"Asserts value cannot be hashed with 0 string",
			[]byte(""),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.ApplySha256Bytes(tt.data); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.data, got)
			}
		})
	}
}

func Test_security_CreatePassword(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name     string
		password string
	}{
		{
			"Asserts input value is accepted (len 8)",
			"test1234",
		},
		{
			"Asserts input value is not accepted (len 7)",
			"test123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.CreatePassword(tt.password); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.password, got)
			}
		})
	}
}

func Test_security_GenerateAccessToken(t *testing.T) {
	s := NewSecurity(nil)

	first := s.GenerateAccessToken()
	second := s.GenerateAccessToken()
	third := s.GenerateAccessToken()

	assert.NotEqual(t, first, second)
	assert.NotEqual(t, second, third)
	assert.NotEqual(t, third, first)
}


func Test_security_ValidatePassword(t *testing.T) {
	s := NewSecurity(nil)

	type args struct {
		hashed string
		input  string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Assert that input and hashed match",
			args{
				hashed: "$2a$13$w.V2eUWfOpm4hc0.a0J41eD9NKKTJj/scNgT6u.C12UOzRtthoUxy",
				input:  "Test1234",
			},
			true,
		},
		{
			"Assert that input and hashed DO NOT match",
			args{
				hashed: "$2a$13$w.V2eUWfOpm4hc0.a0J41eD9NKKTJj/scNgT6u.C12UOzRtthoUxy",
				input:  "Geronimo",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := s.ValidatePassword(tt.args.hashed, tt.args.input); got != tt.want {
				t.Errorf("ValidatePassword() = %v, want %v", got, tt.want)
			}
		})
	}
}
