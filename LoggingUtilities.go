// Copyright Jack Roden (jroden2) 2022

package jutilities

import (
	"fmt"
	"github.com/rs/zerolog"
	"os"
)

func NewLogger() *zerolog.Logger {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
	return &logger
}

// Log creates a log of a passed interface
//
// value is any interface passed
//
// logger is any already present *zerolog.Logger, or else instantiates a new one
func Log(value interface{}, logger *zerolog.Logger) string {
	return logBase("log", value, logger)
}

// Info creates a log as Info of a passed interface
//
// value is any interface passed
//
// logger is any already present *zerolog.Logger, or else instantiates a new one
func Info(value interface{}, logger *zerolog.Logger) string {
	return logBase("info", value, logger)
}

// Warn creates a log as Warning of a passed interface
//
// value is any interface passed
//
// logger is any already present *zerolog.Logger, or else instantiates a new one
func Warn(value interface{}, logger *zerolog.Logger) string {
	return logBase("warn", value, logger)
}

// Error creates a log as Error of a passed interface
//
// value is any interface passed
//
// logger is any already present *zerolog.Logger, or else instantiates a new one
func Error(value interface{}, logger *zerolog.Logger) string {
	return logBase("error", value, logger)
}

// Err creates a log as Error of a passed error
//
// value is any interface passed
//
// logger is any already present *zerolog.Logger, or else instantiates a new one
func Err(err error, logger *zerolog.Logger) string {
	return logBase("error", err.Error(), logger)
}

func logBase (logType string, value interface{}, logger *zerolog.Logger) string {
	if logger == nil {
		logger = NewLogger()
	}

	logValue := fmt.Sprint(value)
	switch logType{
		case "log":
			logger.Log().Msg(logValue)
			return logValue
		case "info":
			logger.Info().Msg(logValue)
			return logValue
		case "warn":
			logger.Warn().Msg(logValue)
			return logValue
		case "error":
			logger.Error().Msg(logValue)
			return logValue
	}
	return fmt.Sprintf("Log type doesn't match %s", logType)
}

