package jutilities

import (
	"github.com/pquerna/otp/totp"
	"github.com/rs/zerolog"
	"os"
)

type otp struct {
	logger *zerolog.Logger
}

func NewOtp(logger *zerolog.Logger) Otp {
	if logger == nil {
		logger = NewLogger()
	}

	return &otp{
		logger:      logger,
	}
}

type Otp interface {
	GenerateTotp(issuer, accountName string) string
	VerifyTotp(TOTP string) bool
}

func (u *otp) GenerateTotp(issuer, accountName string) string {
	secret := os.Getenv("OTP_SEED")
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      issuer,
		AccountName: accountName,
		Secret: []byte(secret),
	})
	if err != nil {
		u.logger.Error().Msg(err.Error())
		return ""
	}

	return key.URL()
}

func (u *otp) VerifyTotp(TOTP string) bool {
	return totp.Validate(TOTP, "JE2VQV2FGJFUCS2OKNME2WSMJ5DVUMSIJ4ZVSPI")
}