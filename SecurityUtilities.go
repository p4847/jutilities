package jutilities

import (
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"golang.org/x/crypto/bcrypt"
)

type security struct {
	MinimumCost int
	logger *zerolog.Logger
}

func NewSecurity(logger *zerolog.Logger) Security {
	if logger == nil {
		logger = NewLogger()
	}

	return &security{
		MinimumCost: 10,
		logger:      logger,
	}
}

type Security interface {
	GenerateAccessToken() string
	GenerateOtp(issuer, recipient string) string
	ValidateOtp(otp string) bool
	ApplySha256Bytes(data []byte) (string, error)
	ApplySha256(data string) (string, error)
	CreatePassword(password string) (string, error)
	ValidatePassword(hashed, input string) bool
}

func (s *security) GenerateAccessToken() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func (s *security) GenerateOtp(issuer, recipient string) string {
	return NewOtp(s.logger).GenerateTotp(issuer, recipient)
}

func (s *security) ValidateOtp(otp string) bool {
	return NewOtp(s.logger).VerifyTotp(otp)
}

// ApplySha256Bytes creates a hash of byte array
func (s *security) ApplySha256Bytes(data []byte) (string, error) {
	if len(data) <= 0 {
		return "", errors.New("data cannot be nil")
	}
	sum256 := sha256.Sum256(data)
	return fmt.Sprintf("%x", sum256), nil
}

// ApplySha256 creates a hash of string data
func (s *security) ApplySha256(data string) (string, error) {
	return s.ApplySha256Bytes([]byte(data))
}

// CreatePassword generates a password with the use of bcrypt lib
func (s *security) CreatePassword(password string) (string, error) {
	if len(password) >= 8 {
		hashed, err := bcrypt.GenerateFromPassword([]byte(password), s.MinimumCost)
		if err != nil {
			fmt.Println(err.Error())
			return "", err
		}
		return string(hashed), nil
	}
	return "", errors.New("password cannot be less than 8 chars")
}

// ValidatePassword validates the input against hashed
func (s *security) ValidatePassword(hashed, input string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(input)); err != nil {
		return false
	}
	return true
}